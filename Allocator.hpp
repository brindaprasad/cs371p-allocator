// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument
#include <iostream>
#include "gtest/gtest.h"

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {

    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

public:
    // ---------------
    // iterator
    // over the blocks
    // ---------------

    class iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            return (*lhs == *rhs);
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;


    public:
        // -----------
        // constructor
        // -----------

        iterator (int* p) {
            _p = p;
        }



        // ----------
        // operator *
        // ----------

        int operator * () const {

            int* another_ptr = (int*)_p;
            return *_p;
        }           // replace!

        // -----------
        // operator ++
        // -----------

        iterator& operator ++ () {
            int* another_ptr = (int*)_p;
            int size = *another_ptr;
            if(size < 0) {
                size = size * -1;
            }
            char* char_p = (char*)_p;
            char_p += size + 8;
            _p = (int*)char_p;

            return *this;

        }

        // -----------
        // operator ++
        // -----------

        iterator operator ++ (int) {
            iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        iterator& operator -- () {
            char* char_p = (char*)_p;
            int prev_size = *(int*)(char_p);
            if(prev_size < 0) {
                prev_size = prev_size * -1;
            }

            char_p = char_p - prev_size - 8;
            _p = (int*)char_p;
            return *this;
        }

        // -----------
        // operator --
        // -----------

        iterator operator -- (int) {
            iterator x = *this;
            --*this;
            return x;
        }
    };

    // ---------------
    // const_iterator
    // over the blocks
    // ---------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            return *lhs == *rhs;
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        const int* _p;

    public:
        // -----------
        // constructor
        // -----------

        const_iterator (const int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        const int& operator * () const {
            int* another_ptr = (int*)_p;
            return *_p;
        }

        // -----------
        // operator ++
        // -----------

        /* go from the beginning sentinel of one block to beginning sentinel of next block */
        const_iterator& operator ++ () {
            int* another_ptr = (int*)_p;
            int size = *another_ptr;
            if(size < 0) {
                size = size * -1;
            }
            char* char_p = (char*)_p;
            char_p += size + 8;
            _p = (int*)char_p;
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        /* go from the end sentinel of one block to end sentinel of previous block */
        const_iterator& operator -- () {
            char* char_p = (char*)_p;
            int prev_size = *(int*)(char_p);
            if(prev_size < 0) {
                prev_size = prev_size * -1;
            }

            char_p = char_p - prev_size - 8;
            _p = (int*)char_p;
            return *this;
        }

        // -----------
        // operator --
        // -----------

        const_iterator operator -- (int) {
            const_iterator x = *this;
            --*this;
            return x;
        }
    };

private:
    // ----
    // data
    // ----

    char a[N];

    FRIEND_TEST(AllocatorFixture, test16);
    FRIEND_TEST(AllocatorFixture, test17);
    FRIEND_TEST(AllocatorFixture, test18);

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * makes sure that sentinels match, able get to the
     * end properly, and that no two free blocks are next
     * to each other
     */

    bool valid () const {

        const_iterator i = begin();

        int index = 0;

        bool prev_is_free = false;

        /*check if its going to the end */
        while(i != end()) {
            int size = *i;
            if(size > 0) {
                if(prev_is_free) {
                    /* two free blocks in a row (not merged) */
                    return false;
                } else {
                    prev_is_free = true;
                }
            } else {
                prev_is_free = false;
                size = size * -1;
            }
            int end_size = *(int*)(&a[index + size + 4]);
            if(end_size < 0) {
                end_size = end_size * -1;
            }

            /* sentinels don't match */
            if(size != end_size) {

                return false;
            }

            index += size + 8;

            ++i;

        }

        return true;
    }

public:
    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () {
        using namespace std;
        (*this)[0] =  (N - 8);
        (*this)[N-4] = (N - 8);
        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     */
    pointer allocate (size_type num) {

        /* can't allocate this amount */
        if(num > (1000/sizeof(T)) || num <= 0) {
            throw std::bad_alloc();
        }


        bool is_alloc = false;


        int index = 0;

        /* only search through array */
        while(index < N) {

            int blocksize = *(int*)(&a[index]);
            /* if block is free */
            if(*(int*)(&a[index]) > 0) {

                int total_size = num * sizeof(T);

                /* can the block hold how much we need to allocate */
                if (*(int*)(&a[index]) >= total_size) {
                    /* can the block be split into an allocated and free block */
                    if(*(int*)(&a[index]) - total_size >= 8 + sizeof(T)) {

                        int orig_size = *(int*)(&a[index]);
                        /* allocated beg */
                        *(int*)(&a[index]) = -1 * total_size;
                        /* allocated end */
                        *(int*)(&a[index + total_size + 4]) = -1 * total_size;
                        int new_size = (orig_size - 8 - total_size);
                        /* free beginning */
                        *(int*)(&a[index + total_size + 8]) =  new_size;
                        /* free end */
                        *(int*)(&a[index + total_size + 12 + (new_size)]) =  new_size;
                        /* we have allocated */
                        is_alloc = true;
                        break;

                    } else {
                        /* need to allocate the whole block */
                        int fbsize = *(int*)(&a[index]);

                        *(int*)(&a[index]) = fbsize * -1;

                        *(int*)(&a[index + fbsize + 4]) = fbsize * -1;
                        is_alloc = true;
                        break;
                    }
                }
                /* move to the next block, not big enough */
                index += blocksize + 8;
            } else {
                /* move to the next block, this one is allocated */
                index += -1 * blocksize + 8;
            }
        }

        /* we couldn't find a large enough free block */
        if(!is_alloc) {
            throw std::bad_alloc();
        }

        assert(valid());
        /* returns index to the beg of the newly allocated block (not beg of sentinel) */
        return (T*)(&a[index + 4]);
    }


    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     */
    void deallocate (pointer p, size_type size) {

        char* char_p = (char*) p;

        int* aint = (int*)a;

        /* throw invalid argument if out of scope */

        if((&*(int*)(char_p) - 1 < &(*aint))  || (&*(int*)(char_p) + 1 + (size /4) > &*(int*)(&a[N-1]) + 1)) {
            throw std::invalid_argument("Invalid pointer");
        }

        char* beg_block = char_p - 4;

        /* no merge */
        if(((&*(int*)(char_p) - 1 == &(*aint)) || (*(int*)(char_p - 8) < 0)) && ((&*(int*)(char_p) + 1 + (size /4) >= &*(int*)(&a[N-1])) || (*(int*)(char_p + size + 4) < 0))) {
            *(int*)(char_p - 4) = size;
            *(int*)(char_p + size) = size;
        }

        /* merge left */
        else if ((&*(int*)(char_p) + 1 + (size /4) >= &*(int*)(&a[N-1])) || (*(int*)(char_p + size + 4) < 0)) {
            char* orig_beg = char_p - 4;
            char* orig_prev_end = char_p - 8;
            int orig_prev_size = (*(int*)(orig_prev_end));
            char* beg = orig_prev_end - orig_prev_size - 4;

            char* end = orig_beg + 4 + size;

            *(int*)(beg) = orig_prev_size + size +  8;
            *(int*)(end) = orig_prev_size + size + 8;


        }

        /* merge right */
        else if ((&*(int*)(char_p) - 1 == &(*aint)) || (*(int*)(char_p - 8) < 0))  {
            char* beg = char_p - 4;
            char* orig_next_beg = beg + size + 8;
            int orig_next_size = (*(int*)(orig_next_beg));

            char* end = orig_next_beg + orig_next_size + 4;

            *(int*)(beg) = orig_next_size + size +  8;
            *(int*)(end) = orig_next_size + size + 8;
        }

        /* merge both */
        else {

            /* merge left */
            char* orig_beg = char_p - 4;
            char* orig_prev_end = char_p - 8;
            int orig_prev_size = (*(int*)(orig_prev_end));
            char* beg = orig_prev_end - orig_prev_size - 4;

            /* merge right */
            char* orig_next_beg = orig_beg + size + 8;
            int orig_next_size = (*(int*)(orig_next_beg));

            char* end = orig_next_beg + orig_next_size + 4;

            /* merges it all together */
            *(int*)(beg) = orig_prev_size + orig_next_size + size +  16;
            *(int*)(end) = orig_prev_size + orig_next_size + size + 16;
        }

        assert(valid());
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator begin () {
        return iterator(&(*this)[0]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator begin () const {
        return const_iterator(&(*this)[0]);
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator end () {
        return iterator(&(*this)[N]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator end () const {
        return const_iterator(&(*this)[N]);
    }
};

#endif // Allocator_h
