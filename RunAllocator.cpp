// ----------------
// RunAllocator.c++
// ----------------

// --------
// includes
// --------

#include <iostream> // cin, cout

#include "Allocator.hpp"

// ----
// main
// ----

int main () {
    using namespace std;

    string s;
    int t;
    cin >> t;
    getline(cin, s);
    assert(t >= 0 && t <= 100);

    assert(s.empty());

    getline(cin, s);
    // solve for each test case
    for(int i = 0; i < t; ++i) {
        my_allocator<double, 1000> alloc;

        getline(cin, s);

        int num;

        do {

            num = atoi(s.c_str());
            /* make sure you can't allocate 0 */
            if(num == 0)
                break;

            if(num > 0) {
                alloc.allocate(num);
            } else {
                /* find ptr to ith block + deallocate */
                my_allocator<double, 1000>::iterator it = &alloc[0];
                int count = 0;
                int index = 0;
                while(it != alloc.end()) {

                    if(*it < 0) {
                        count++;
                        if(count == (-1*num)) {
                            int size = alloc[index];
                            if(alloc[index] < 0) {
                                size = -1 * size;
                            }

                            alloc.deallocate((double*)&alloc[index + 4], size);
                            break;
                        } else {
                            index += (-1 * *it) + 8;
                            ++it;
                        }
                    } else {

                        index += *it + 8;
                        ++it;
                    }
                }
            }


        } while(getline(cin, s));


        my_allocator<double, 1000>::iterator iter = &alloc[0];

        while (iter != alloc.end()) {
            cout << *iter;
            ++iter;
            if (iter!= alloc.end()) {
                cout << " ";
            }
        }
        cout << endl;
    }
    return 0;
}
