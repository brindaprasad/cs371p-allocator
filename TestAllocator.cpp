// -----------------
// TestAllocator.c++
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <cstddef>   // ptrdiff_t
#include <memory>    // allocator

#include "gtest/gtest.h"

#include "Allocator.hpp"



/* initialization */
TEST(AllocatorFixture, test0) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    ASSERT_EQ(x[0], 992);
}


/* const allocator */
TEST(AllocatorFixture, test1) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    const allocator_type x;
    ASSERT_EQ(x[0], 992);
}

/* allocating 10 doubles */
TEST(AllocatorFixture, test2) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    x.allocate(10);

    ASSERT_EQ(x[0], -80);
    ASSERT_EQ(x[84], -80);
}


/* allocating 20 doubles */
TEST(AllocatorFixture, test3) {
    using allocator_type = my_allocator<char, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    x.allocate(20);

    ASSERT_EQ(x[0], -20);
    ASSERT_EQ(x[24], -20);
}


/* allocate 20, then deallocate */
TEST(AllocatorFixture, test4) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    x.allocate(20);
    x.deallocate((int*)&x[4], 80);

    ASSERT_EQ(x[0], 992);
    ASSERT_EQ(x[996], 992);
}


/* allocate 20, 50, deallocate 50 */
TEST(AllocatorFixture, test5) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    x.allocate(20);
    x.allocate(50);
    x.deallocate((int*)&x[92], 200);

    ASSERT_EQ(x[88], 904);
    ASSERT_EQ(x[996], 904);
}

/* allocate 20, 50, deallocate 50, preincrement */
TEST(AllocatorFixture, test6) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    my_allocator<double, 1000>::iterator it = &x[0];
    x.allocate(20);
    x.allocate(50);
    x.deallocate((int*)&x[92], 200);

    ASSERT_EQ(*it, -80);
    ++it;
    ASSERT_EQ(*it, 904);
}

/* 20 50 50, predecrement */
TEST(AllocatorFixture, test7) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    my_allocator<double, 1000>::iterator it = &x[996];
    x.allocate(20);
    x.allocate(50);
    x.deallocate((int*)&x[92], 200);

    ASSERT_EQ(*it, 904);
    --it;
    ASSERT_EQ(*it, -80);
}

/* same as before but with const */
TEST(AllocatorFixture, test8) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    my_allocator<double, 1000>::const_iterator it = &x[996];
    x.allocate(20);
    x.allocate(50);
    x.deallocate((int*)&x[92], 200);

    ASSERT_EQ(*it, 904);
    --it;
    ASSERT_EQ(*it, -80);
}

/* 20 50 50 post increment */
TEST(AllocatorFixture, test9) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    my_allocator<double, 1000>::iterator it = &x[0];
    x.allocate(20);
    x.allocate(50);
    x.deallocate((int*)&x[92], 200);

    ASSERT_EQ(*it, -80);
    it++;
    ASSERT_EQ(*it, 904);
}

/* 20 50 50 post decrement */
TEST(AllocatorFixture, test10) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    my_allocator<double, 1000>::iterator it = &x[996];
    x.allocate(20);
    x.allocate(50);
    x.deallocate((int*)&x[92], 200);

    ASSERT_EQ(*it, 904);
    it--;
    ASSERT_EQ(*it, -80);
}

/* 15 35 alloc/dealloc */
TEST(AllocatorFixture, test11) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    my_allocator<double, 1000>::iterator it = &x[0];
    x.allocate(15);
    x.allocate(35);
    x.deallocate((int*)&x[68], 140);

    ASSERT_EQ(*it, -60);
}

/* 71 50 */
TEST(AllocatorFixture, test12) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    my_allocator<double, 1000>::iterator it = &x[0];
    x.allocate(71);
    x.allocate(50);
    x.deallocate((int*)&x[4], 284);

    ASSERT_EQ(x[292], -200);
}

/* 200 */
TEST(AllocatorFixture, test13) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    my_allocator<int, 1000>::iterator it = &x[0];
    pointer p = x.allocate(200);

    ASSERT_EQ(x[0], -800);
}

/* != it */
TEST(AllocatorFixture, test14) {
    using allocator_type = my_allocator<short, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    my_allocator<short, 1000>::iterator it = &x[0];
    my_allocator<short, 1000>::iterator ittwo = &x[0];
    while(it != x.end()) {
        it++;
    }

    ASSERT_EQ(it != ittwo, true);
}

/* == it */
TEST(AllocatorFixture, test15) {
    using allocator_type = my_allocator<short, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    my_allocator<short, 1000>::iterator it = &x[0];
    my_allocator<short, 1000>::iterator ittwo = &x[0];
    while(it != x.end()) {
        it++;
    }
    while(ittwo != x.end()) {
        ittwo++;
    }

    ASSERT_EQ(it == ittwo, true);
}

/* valid - constructor */
TEST(AllocatorFixture, test16) {
    using allocator_type = my_allocator<short, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;

    ASSERT_EQ(x.valid(), true);
}

/* allocate valid */
TEST(AllocatorFixture, test17) {
    using allocator_type = my_allocator<short, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;

    x.allocate(10);

    ASSERT_EQ(x.valid(), true);
}

/* valid allocate + deallocate */
TEST(AllocatorFixture, test18) {
    using allocator_type = my_allocator<short, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;

    pointer p = x.allocate(10);
    x.deallocate(p, 20);

    ASSERT_EQ(x.valid(), true);
}

/* allocate and deallocate 50 + 3 */
TEST(AllocatorFixture, test19) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    x.allocate(50);
    x.allocate(3);

    ASSERT_EQ(x[0], -400);
    ASSERT_EQ(x[408], -24);
}