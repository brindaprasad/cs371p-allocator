# CS371p: Object-Oriented Programming Allocator Repo

* Name: Richa Gadre, Brinda Prasad

* EID: rkg723, bp24664

* GitLab ID: richagadre, brindaprasad

* HackerRank ID: richagadre

* Git SHA: d544aca979dd3737c1139ff0914fa49b18cf7408

* GitLab Pipelines: https://gitlab.com/brindaprasad/cs371p-allocator/-/pipelines

* Estimated completion time: 14 hours

* Actual completion time: 16 hours

* Comments: We worked together >75% of the time!
